#ifndef THESEUS_FORMATS_YUV_H_
#define THESEUS_FORMATS_YUV_H_

#include <array>
#include <fstream>

#include "common/types.h"
#include "structures/frameDescriptor.h"


namespace theseus::formats {

class Yuv {
public:
	const structures::FrameDescriptor frame_desc;

	Yuv(structures::FrameDescriptor);

	/*
		parameters:
			pixel*: filled with data extracted according to parameters
			frame_idx: frame position
			ChannelType: The channel
			ifstream: File (in Yuv format)
	*/
	void read(pixel* container, unsigned frame_idx, ChannelType, std::ifstream&) const;

private:
	struct ChannelDescriptor {
		ChannelDescriptor(
			size_t width, size_t height,
			unsigned start_pos,
			size_t channel_size,
			size_t frame_size
		);

		ChannelDescriptor() = default;

		unsigned file_index(unsigned frame_idx) const;

		size_t width{0};
		size_t height{0};
		unsigned start_pos{0};
		size_t frame_size{0};
		size_t channel_size{0};
	};

	std::array<ChannelDescriptor, 3> channel_descriptors_{};
};

}

#endif //THESEUS_FORMATS_YUV_H_
