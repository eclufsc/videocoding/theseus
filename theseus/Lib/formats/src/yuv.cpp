#include "formats/yuv.h"

namespace theseus::formats {

Yuv::Yuv(structures::FrameDescriptor fd):
	frame_desc{fd}
{
	size_t lumma_size = frame_desc.width*frame_desc.height;
	size_t chroma_size = frame_desc.chroma_width*frame_desc.chroma_height;
	size_t frame_size = lumma_size + 2*chroma_size;
	// lumma channel
	unsigned start_pos = 0;
	channel_descriptors_[0] = ChannelDescriptor(
		frame_desc.width, frame_desc.height,
		start_pos,
		lumma_size,
		frame_size
	);
	// chroma cb channel
	start_pos += lumma_size;
	channel_descriptors_[1] = ChannelDescriptor(
		frame_desc.chroma_width, frame_desc.chroma_height,
		start_pos,
		chroma_size,
		frame_size
	);
	// chroma cr channel
	start_pos += chroma_size;
	channel_descriptors_[2] = ChannelDescriptor(
		frame_desc.chroma_width, frame_desc.chroma_height,
		start_pos,
		chroma_size,
		frame_size
	);
}

void Yuv::read(pixel* container, unsigned frame_idx, ChannelType ch, std::ifstream& file) const {
	const ChannelDescriptor *channel = &channel_descriptors_[ch];
	unsigned file_position = channel->file_index(frame_idx);
	size_t size = channel->channel_size;
	file.seekg(file_position);
	file.read(reinterpret_cast<char*>(container), size);
}

Yuv::ChannelDescriptor::ChannelDescriptor(
	size_t width, size_t height,
	unsigned start_pos,
	size_t channel_size,
	size_t frame_size
):
	width(width), height(height),
	start_pos(start_pos),
	channel_size(channel_size),
	frame_size(frame_size)
{}

unsigned Yuv::ChannelDescriptor::file_index (unsigned frame_idx) const {
	return frame_idx*frame_size + start_pos;
}

}
