#ifndef THESEUS_STRUCTURES_BLOCKITERATOR_INC_
#define THESEUS_STRUCTURES_BLOCKITERATOR_INC_

template <bool is_const>
class BlockIterator {
public:
	using container = typename choose<is_const, const Block&, Block&>::type;

	using iterator_category = std::random_access_iterator_tag;
	using value_type = pixel;
	using difference_type = std::ptrdiff_t;
	using pointer = typename choose<is_const, const pixel*, pixel*>::type;
	using reference = typename choose<is_const, const pixel&, pixel&>::type;

	BlockIterator(container origin, int position):
		block_(origin),
		x_(position % origin.width),
		y_(position / origin.width)
	{}

	BlockIterator& operator+=(int val) {
		window_aware_move(val, block_.width, y_, x_);
		return *this;
	}

	reference operator[](int offset) const {
		int i = y_;
		int j = x_;
		window_aware_move(offset, block_.width, i, j);
		return block_[i][j];
	}

	reference operator*() const {
		return *operator->();
	}

	pointer operator->() const {
		return block_[y_] + x_;
	}

	BlockIterator& operator++() {
		return this->operator+=(1);
	}

	BlockIterator& operator--() {
		return this->operator-=(1);
	}

	BlockIterator& operator-=(int val) {
		return this->operator+=(-val);
	}

	friend BlockIterator operator+(BlockIterator lhs, int rhs) {
		return lhs += rhs;
	}

	friend BlockIterator operator-(BlockIterator lhs, int rhs) {
		return lhs -= rhs;
	}

	friend difference_type operator-(const BlockIterator& lhs, const BlockIterator& rhs) {
		assert(&lhs.block_ == &rhs.block_);
		return (lhs.y_ - rhs.y_)*lhs.block_.width + (lhs.x_ - rhs.x_);
	}

	friend bool operator==(const BlockIterator& lhs, const BlockIterator& rhs) {
		return (&lhs.block_ == &rhs.block_) && (lhs.y_ == rhs.y_) && (lhs.x_ == rhs.x_);
	}

	friend bool operator!=(const BlockIterator& lhs, const BlockIterator& rhs) {
		return !(lhs == rhs);
	}

	friend bool operator<(const BlockIterator& lhs, const BlockIterator& rhs) {
		assert(&lhs.block_ == &rhs.block_);
		return (lhs.y_*lhs.block_.width + lhs.x_) < (rhs.y_*rhs.block_.width + rhs.x_);
	};

	friend bool operator<=(const BlockIterator& lhs, const BlockIterator& rhs) {
		return (lhs == rhs) || (lhs < rhs);
	};

	friend bool operator>(const BlockIterator& lhs, const BlockIterator& rhs) {
		return !(lhs <= rhs);
	}

	friend bool operator>=(const BlockIterator& lhs, const BlockIterator& rhs) {
		return !(lhs < rhs);
	}

private:
	int y_;
	int x_;
	container block_;

	static void window_aware_move(int offset, size_t width, int& y, int& x) {
		y += (offset / width);

		x += (offset % width);
		if (x >= width)
			y += 1;
		else if (x < 0)
			y -= 1;

		x %= width;
	}
};

#endif
