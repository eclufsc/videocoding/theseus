#ifndef THESEUS_STRUCTURES_FRAMEDESCRIPTOR_H_
#define THESEUS_STRUCTURES_FRAMEDESCRIPTOR_H_

#include "common/types.h"

namespace theseus::structures {

struct FrameDescriptor {
	const size_t width;
	const size_t height;
	const Subsampling subsampling;
	const size_t chroma_width;
	const size_t chroma_height;

	FrameDescriptor(
		size_t width, size_t height,
		Subsampling subsampling
	);

private:
	// Calculate width and height sizes of chroma channels according to Subsampling
	size_t chroma_w() const;
	size_t chroma_h() const;
};

}

#endif //THESEUS_STRUCTURES_FRAMEDESCRIPTOR_H_
