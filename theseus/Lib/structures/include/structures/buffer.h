#ifndef THESEUS_STRUCTURES_BUFFER_H_
#define THESEUS_STRUCTURES_BUFFER_H_

#include <cassert>
#include <memory>
#include <vector>

#include "common/types.h"


namespace theseus::structures {

template<class Type>
class Buffer {
public:
	Buffer(size_t max_elements, size_t data_size):
		max_elements_{max_elements},
		next_position_{0}
	{
		data_.reserve(max_elements_);
		for (int i =0; i < max_elements_; i++)
			data_.push_back(std::make_unique<Type[]>(data_size));
	}

	Buffer():
		next_position_{0}
	{}

	/*
		return:
			Type*: next dataframe to be written
		obs: 
			Use this function when you need to WRITE a new frame into the buffer
			It always replace the oldest dataframe
	*/
	Type* next() {
		int idx = next_position_;
		next_position_ = (next_position_+1)%max_elements_;
		return data_[idx].get();
	}

	/*
		Parameter:
			int idx: An index to access a dataframe
		return:
			Type*: the dataframe at idx position
		obs: 
			Use this function when you need to READ a frame from the buffer
			This function is useless if you handle the Type* at application level after use "Type* next()" 
	*/
	const Type* at(int idx) const {
		assert(idx >= 0);
		assert(idx < max_elements_);
		return data_[idx].get();
	}

private:
	std::vector<std::unique_ptr<Type[]>> data_;
	size_t max_elements_;
	int next_position_;
};

}

#endif
