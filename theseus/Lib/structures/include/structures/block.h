#ifndef THESEUS_STRUCTURES_BLOCK_H_
#define THESEUS_STRUCTURES_BLOCK_H_

#include <iterator>
#include <cstddef>

#include "common/types.h"


namespace theseus::structures {

struct Block {
	const size_t height, width;
	const size_t vertical_stride;
	pixel* data;

	Block(
		pixel* data, // Block base location (upper left corner).
		size_t width, size_t height,
		size_t vertical_stride // Distance between two block rows.
	);

	// A frame-sized block defaults the vertical stride to its width.
	Block(pixel* data, size_t width, size_t height);

	const pixel* operator[](int row) const;
	pixel* operator[](int row);

	// Returns the block's resolution.
	size_t size() const;

	#include "structures/blockIterator.inc"

	BlockIterator<false> begin();
	BlockIterator<false> end();
	BlockIterator<true> begin() const;
	BlockIterator<true> end() const;
};

}

#endif // THESEUS_STRUCTURES_BLOCK_H_
