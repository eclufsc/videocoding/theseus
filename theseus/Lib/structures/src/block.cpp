#include "structures/block.h"

#include <cassert>
#include <cstddef>

#include "common/types.h"


namespace theseus::structures {

Block::Block(
	pixel* data,
	size_t width, size_t height,
	size_t vertical_stride
):
	data(data),
	width(width), height(height),
	vertical_stride(vertical_stride)
{
	assert(data != nullptr);
	assert(width > 0);
	assert(height > 0);
	assert(vertical_stride > 0);
}

Block::Block(pixel* data, size_t width, size_t height):
	data(data),
	width(width), height(height),
	vertical_stride(width)
{
	assert(data != nullptr);
	assert(width > 0);
	assert(height > 0);
}

const pixel* Block::operator[](int row) const {
	assert(row >= 0);
	assert(row < height);
	return data + (row * vertical_stride);
}

pixel* Block::operator[](int row) {
	return const_cast<pixel*>(const_cast<const Block&>(*this)[row]);
}

size_t Block::size() const {
	return width * height;
}

Block::BlockIterator<false> Block::begin() {
	return Block::BlockIterator<false>(*this, 0);
}

Block::BlockIterator<false> Block::end() {
	return Block::BlockIterator<false>(*this, size());
}

Block::BlockIterator<true> Block::begin() const {
	return Block::BlockIterator<true>(*this, 0);
}

Block::BlockIterator<true> Block::end() const {
	return Block::BlockIterator<true>(*this, size());
}

}
