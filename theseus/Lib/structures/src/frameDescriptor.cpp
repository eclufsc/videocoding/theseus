#include "structures/frameDescriptor.h"

namespace theseus::structures {

FrameDescriptor::FrameDescriptor(
	size_t width, size_t height,
	Subsampling subsampling
):
	width{width}, height{height},
	subsampling{subsampling},
	chroma_width{chroma_w()},
	chroma_height{chroma_h()}
{}

size_t FrameDescriptor::chroma_w() const {
	switch (subsampling) {
		case c440 : return width; //full horizontal resolution
		case c422 : return width>>1; //half horizontal resolution
		case c420 : return width>>1; //half horizontal resolution
		case c411 : return width>>2; //quarter horizontal resolution
		default : return width; //full horizontal resolution
	}
};

size_t FrameDescriptor::chroma_h() const {
	switch (subsampling) {
		case c422 : return height; //full vertical resolution
		case c411 : return height; //full vertical resolution
		case c440 : return height>>1; //half vertical resolution
		case c420 : return height>>1; //half vertical resolution
		default : return height; //full vertical resolution
	}
};

}
