#ifndef THESEUS_COMMON_TYPES_H_
#define THESEUS_COMMON_TYPES_H_

#include <cstdint>
#include <ostream>
#include <cstddef>


namespace theseus {

using pixel = std::uint8_t;

enum Subsampling {
	c444, c440, c422, c420, c411
};

enum ChannelType {
	luma, chroma_b, chroma_r
};


/*
useful template to avoid code duplication implementing const & non-const versions,
see https://stackoverflow.com/questions/2150192/how-to-avoid-code-duplication-implementing-const-and-non-const-iterators
example usage:
	constexpr is_const = true; // defines whether or not the type will be constant
	using generic_int = typename choose<is_const, const int, int>::type; // generic_int is actually a const int, since is_const is true
*/
template <bool flag, class IsTrue, class IsFalse>
struct choose;

template <class IsTrue, class IsFalse>
struct choose<true, IsTrue, IsFalse> {
	typedef IsTrue type;
};

template <class IsTrue, class IsFalse>
struct choose<false, IsTrue, IsFalse> {
	typedef IsFalse type;
};

}


// since ostreams interpret 8-bit data as characters, one must override this
std::ostream& operator<<(std::ostream& out, const theseus::pixel& px);


#endif // THESEUS_COMMON_TYPES_H_
