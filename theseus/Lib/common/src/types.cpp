#include "common/types.h"

#include <iomanip>


std::ostream& operator<<(std::ostream& out, const theseus::pixel& px) {
	out << std::setw(3) << static_cast<unsigned>(px);
	return out;
}
