#ifndef MODEL_ENCODER_INPUTFRAMES_H
#define MODEL_ENCODER_INPUTFRAMES_H

#include <fstream>
#include <utility>
#include <cassert>

#include <common/types.h>
#include <structures/block.h>
#include <structures/frameDescriptor.h>
#include <structures/buffer.h>

#include "common/frame.h"


namespace model::encoder {
	using namespace theseus;

	template<class Format, size_t buffer_size>
	class InputFrames {
	public:
		InputFrames(
			std::ifstream file,
			size_t sequence_size,
			structures::FrameDescriptor f_desc
		) :
			file_(std::move(file)),
			sequence_size_(sequence_size),
			frame_desc_(f_desc),
			format_(Format(f_desc))
		{
			y_buffer_ = structures::Buffer<pixel>(buffer_size, frame_desc_.width * frame_desc_.height);
			cb_buffer_ = structures::Buffer<pixel>(buffer_size, frame_desc_.chroma_width *  frame_desc_.chroma_height);
			cr_buffer_ = structures::Buffer<pixel>(buffer_size, frame_desc_.chroma_width *  frame_desc_.chroma_height);
		}

		InputFrames() = delete;

		common::Frame read_frame(int f_idx) {
			assert(f_idx < sequence_size_);
			structures::Block y_block = read_channel(y_buffer_, f_idx, ChannelType::luma, frame_desc_.width, frame_desc_.height);
			structures::Block cb_block = read_channel(cb_buffer_, f_idx, ChannelType::chroma_b, frame_desc_.chroma_width, frame_desc_.chroma_height);
			structures::Block cr_block = read_channel(cr_buffer_, f_idx, ChannelType::chroma_r, frame_desc_.chroma_width, frame_desc_.chroma_height);
			return common::Frame(frame_desc_, f_idx, std::move(y_block), std::move(cb_block), std::move(cr_block));
		}

	private:
		std::ifstream file_;
		structures::Buffer<pixel> y_buffer_{}, cb_buffer_{}, cr_buffer_{};
		Format format_;
		structures::FrameDescriptor frame_desc_;
		size_t sequence_size_;

		structures::Block read_channel(
			structures::Buffer<pixel>& buffer,
			int frame_idx,
			ChannelType ch,
			size_t w, size_t h
		) {
			pixel* data_frame = buffer.next();
			format_.read(data_frame, frame_idx, ch, file_);
			return structures::Block{data_frame, w, h};
		}
	};
}

#endif