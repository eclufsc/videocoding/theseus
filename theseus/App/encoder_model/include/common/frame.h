#ifndef MODEL_ENCODER_FRAME_H
#define MODEL_ENCODER_FRAME_H

#include <utility>

#include <structures/block.h>
#include <structures/frameDescriptor.h>


namespace model::common {

using namespace theseus;

class Frame {
public:
	Frame(
		structures::FrameDescriptor d,
		int idx,
		structures::Block y, structures::Block cb, structures::Block cr
	);

private:
	int idx_;
	structures::FrameDescriptor frame_desc_;
	structures::Block y_, cb_, cr_;
};

}

#endif
