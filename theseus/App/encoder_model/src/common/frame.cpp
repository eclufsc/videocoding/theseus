#include "common/frame.h"

namespace model::common {

Frame::Frame(
	structures::FrameDescriptor d,
	int idx,
	structures::Block y, structures::Block cb, structures::Block cr
) :
	frame_desc_(d),
	idx_(idx),
	y_(std::move(y)), cb_(std::move(cb)), cr_(std::move(cr))
{}

}
