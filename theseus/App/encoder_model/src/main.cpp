#include <iostream>
#include <string>
#include <fstream>

#include <formats/yuv.h>

#include "common/frame.h"
#include "encoder/inputFrames.h"

using namespace theseus;

int main() {
	printf("Hello, video World!\n");

	// path in valid when binary is executed from theseus project root folder (with ./bin/encoder_model)
	const std::string video_path = "tests/input_files/w126_h80_f2_s420_bpp8.yuv";
	std::ifstream file;
	file.open(video_path, std::ios::binary);
	if (!file.is_open())
		return -1;

	// Video configurations
	const unsigned width{126};
	const unsigned height{80};
	const Subsampling subsampling = c420;
	const unsigned total_frames{2};

	// Encoder Structures
	structures::FrameDescriptor frame_desc{width, height, subsampling};
	model::encoder::InputFrames<formats::Yuv, 2> input{std::move(file), total_frames, frame_desc};
	model::common::Frame f = input.read_frame(0);
}
