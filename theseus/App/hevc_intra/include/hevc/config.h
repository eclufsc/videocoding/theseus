#ifndef HEVC_CONFIG_H
#define HEVC_CONFIG_H

#include <common/types.h>


namespace hevc {

using theseus::pixel;

short bit_depth();

void bit_depth(short b);

pixel nominal_average();

}

#endif // HEVC_CONFIG_H
