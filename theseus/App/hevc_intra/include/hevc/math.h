#ifndef HEVC_MATH_H
#define HEVC_MATH_H

#include <limits>

namespace hevc::math {

template <typename T, typename U>
inline constexpr T clip(U val, T min, T max) {
	if (val < static_cast<U>(min))
		return min;
	else if (val > static_cast<U>(max))
		return max;
	else
		return val;
}

template <typename T, typename U>
inline constexpr T saturate_cast(U x) {
	return clip(x, std::numeric_limits<T>::min(), std::numeric_limits<T>::max());
}

// linear interpolate: x in [in_min,in_max] -> equivalent y in [out_min,out_max]
template <typename T, typename U>
inline constexpr U lerp_round(T x, T in_min, T in_max, U out_min, U out_max) {
	// the last addend, 0.5*(in_max - in_min), is the rounding factor such that
	// the result a.b -> a when 0.b < 0.5 and a.b -> (a+1) when 0.b >= 0.5; this
	// is equivalent to truncate-rounding (a.b + 0.5)
	return (out_min*(in_max - x) + out_max*(x - in_min) + 0.5*(in_max - in_min)) / (in_max - in_min);
}

}

#endif // HEVC_MATH_H
