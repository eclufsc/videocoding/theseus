#ifndef HEVC_INTRA_H
#define HEVC_INTRA_H

#include <vector>
#include <cstddef>        // size_t

#include <common/types.h> // pixel
#include <structures/block.h>


namespace hevc::intra {

using theseus::pixel;
using theseus::structures::Block;
using std::vector;

enum Mode {
	PLANAR = 0,
	DC = 1,
	ANGULAR_2 = 2,
	ANGULAR_3 = 3,
	ANGULAR_4 = 4,
	ANGULAR_5 = 5,
	ANGULAR_6 = 6,
	ANGULAR_7 = 7,
	ANGULAR_8 = 8,
	ANGULAR_9 = 9,
	ANGULAR_10 = 10,
	ANGULAR_11 = 11,
	ANGULAR_12 = 12,
	ANGULAR_13 = 13,
	ANGULAR_14 = 14,
	ANGULAR_15 = 15,
	ANGULAR_16 = 16,
	ANGULAR_17 = 17,
	ANGULAR_18 = 18,
	ANGULAR_19 = 19,
	ANGULAR_20 = 20,
	ANGULAR_21 = 21,
	ANGULAR_22 = 22,
	ANGULAR_23 = 23,
	ANGULAR_24 = 24,
	ANGULAR_25 = 25,
	ANGULAR_26 = 26,
	ANGULAR_27 = 27,
	ANGULAR_28 = 28,
	ANGULAR_29 = 29,
	ANGULAR_30 = 30,
	ANGULAR_31 = 31,
	ANGULAR_32 = 32,
	ANGULAR_33 = 33,
	ANGULAR_34 = 34,
};

struct Context {
 public:
	Context(const Block& frame, vector<bool> encoded);

	// returns whether or not a sample at position (x,y) is encoded
	bool available(int x, int y) const;

 private:
	Block frame_; // reconstructed (encoded) frame
	vector<bool> encoded_; // which samples in frame are already encoded

	friend class ReferenceSamples;
};

struct ReferenceSamples {
 public:
	ReferenceSamples(
		const Context& context, // information needed by intra prediction
		int x, int y, // predicted block's position relative to the frame
		size_t block_side // predicted block side length
	);

	ReferenceSamples(const ReferenceSamples& origin) = default;

	// access to sample (x,y) in relation to the predicted block
	pixel& at(int x, int y);
	const pixel& at(int x, int y) const;

	// return filtered version of samples
	const ReferenceSamples filter(Mode mode) const;

	// predict a block's contents in a given intra mode
	const ReferenceSamples& predict(Block& block, Mode mode) const;

	// applies post-processing smoothing filters when needed
	const ReferenceSamples& smooth(Block& block, Mode mode) const;

 private:
	vector<pixel> left_;
	vector<pixel> top_;
	pixel corner_;
	const size_t block_side_;

 protected:
	// access to samples as in a one-dimensional reference vector
	const pixel& ref(int pos, Mode mode) const;
};

}

#endif // HEVC_INTRA_H
