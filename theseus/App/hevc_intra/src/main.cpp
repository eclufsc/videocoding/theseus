#include <iostream>       // cout
#include <memory>         // make_unique
#include <vector>
#include <utility>        // move
#include <cstdlib>        // atoi
#include <cstdlib>        // rand

#include <common/types.h> // pixel
#include <structures/block.h>

#include "hevc/intra.h"
#include "hevc/config.h"

// pretty printing for blocks & pixel vectors
std::ostream& operator<<(std::ostream& out, const theseus::structures::Block& block);
std::ostream& operator<<(std::ostream& out, const std::vector<theseus::pixel>& vector);

// purely aesthetic: fill in border contents to be used as reference samples.
// each intra prediction mode output should look like what is seen in
// HEVC: Algorithms and Architectures (2014) figure 4.2 (page 93)
std::vector<bool> fill_borders(theseus::structures::Block& frame);


int main (int argc, char const *argv[]) {
	using namespace hevc::intra;

	// check for block size and prediction mode in program arguments
	if (argc < 3 || (atoi(argv[1]) <= 0) || (atoi(argv[2]) < 0 || 34 < atoi(argv[2]))) {
		std::cerr << "Invalid parameters supplied.\n"
		          << "Expected usage: " << argv[0] << " N" << " M\n"
		          << "  N: predicted block's side length\n"
		          << "  M: Intra prediction mode number (0-34)" << '\n';
		return EXIT_FAILURE;
	}

	// setup parameters
	const auto size = atoi(argv[1]);
	const auto mode = static_cast<hevc::intra::Mode>(atoi(argv[2]));
	hevc::bit_depth(8);

	// prepare an example reference frame proportional to the predicted block
	const auto width = size*2 + 1;
	const auto& height = width;
	auto pixels = std::make_unique<pixel[]>(width*height);

	// build context with information needed by intra prediction
	auto frame = Block(pixels.get(), width, height);
	const auto&& encoded = fill_borders(frame);
	auto context = Context(frame, encoded);

	// generate reference samples for block anchored at frame's (x,y)
	int j = 1, i = 1;
	auto block = Block(&frame[i][j], size, size, frame.width);
	auto samples = ReferenceSamples(context, j, i, size);

	// hevc intra prediction process
	samples.filter(mode)         // pre-processing
	       .predict(block, mode) // prediction
	       .smooth(block, mode); // post-processing

	std::cout << block << '\n';  // output the predicted candidate block

	return EXIT_SUCCESS;
}


std::vector<bool> fill_borders(theseus::structures::Block& frame) {
	static const theseus::pixel corner = 216;
	static const std::vector<theseus::pixel> top = {204, 196, 186, 91, 73, 68, 63, 63, 63, 63, 73, 91, 186, 196, 204, 214};
	static const std::vector<theseus::pixel> left = {214, 204, 196, 191, 186, 91, 73, 68, 63, 63, 63, 63, 73, 91, 186, 196};
	static const unsigned int random_seed = 417;

	// fixed
	if (frame.width == 17 && frame.height == 17) {
		frame[0][0] = corner;

		for (int j = 1; j < frame.width; ++j)
			frame[0][j] = top[j-1];

		for (int i = 1; i < frame.height; ++i)
			frame[i][0] = left[i-1];

	// random generated
	} else {
		std::srand(random_seed);

		for (int i = 0; i < frame.height; ++i)
			for (int j = 0; j < frame.width; ++j)
				frame[i][j] = std::rand() % 256;
	}

	// borders are encoded
	std::vector<bool> encoded(frame.size(), false);

	for (int j = 0; j < frame.width; ++j)
		encoded[0*frame.width + j] = true;

	for (int i = 0; i < frame.height; ++i)
		encoded[i*frame.width + 0] = true;

	return encoded;
}

std::ostream& operator<<(std::ostream& out, const theseus::structures::Block& block) {
	out << "[";
	auto k = 0;
	for (const auto& px : block) {
		out << (k % block.width != 0 ? ", " : k == 0 ? "[ " : " ],\n [ ") << px;
		++k;
	}
	out << " ]]";
	return out;
}

std::ostream& operator<<(std::ostream& out, const std::vector<theseus::pixel>& vector) {
	return out << theseus::structures::Block(const_cast<theseus::pixel*>(vector.data()), vector.size(), 1);
}
