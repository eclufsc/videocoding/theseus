#include "hevc/intra.h"

#include <vector>
#include <cstddef>       // size_t
#include <cassert>
#include <cmath>         // abs
#include <algorithm>     // fill

#include "hevc/config.h" // bit_depth, nominal_average
#include "hevc/math.h"   // lerp_round, saturate_cast


namespace hevc::intra {

Context::Context(const Block& frame, vector<bool> encoded):
	frame_(frame), encoded_(encoded)
{}

bool Context::available(int x, int y) const {
	return (0 <= x && x < frame_.width)
	    && (0 <= y && y < frame_.height)
	    && encoded_[y*frame_.width + x];
}

const pixel& ReferenceSamples::at(int x, int y) const {
	assert(   (x == -1 && y == -1)
	       || (x == -1 && y >= 0)
	       || (y == -1 && x >= 0));
	assert(y < static_cast<int>(left_.size()));
	assert(x < static_cast<int>(top_.size()));

	if (x == -1 && y == -1)
		return corner_;
	else if (x < 0)
		return left_[y];
	else /* if (y < 0) */
		return top_[x];
}

pixel& ReferenceSamples::at(int x, int y) {
	return const_cast<pixel&>(const_cast<const ReferenceSamples&>(*this).at(x, y));
}

ReferenceSamples::ReferenceSamples(const Context& context, int x, int y, size_t block_side):
	block_side_(block_side)
{
	const auto& p = context.frame_;
	const auto& N = block_side_;
	assert(x >= 0);
	assert(x < p.width);
	assert(y >= 0);
	assert(y < p.height);

	// initialize reference samples with nominal average
	const auto average = nominal_average();
	left_ = vector<pixel>(2*N, average);
	top_ = vector<pixel>(2*N, average);
	corner_ = average;

	// if p[2*N-1][-1] is unavailable, rotate clockwise until a valid sample is found
	bool found = false;
	for (int i = 2*N-1; !found && i >= -1; --i) {
		if (context.available(x-1, y+i)) {
			left_[2*N-1] = p[y+i][x-1];
			found = true;
		}
	}
	for (int j = 0; !found && j <= 2*N-1; ++j) {
		if (context.available(x+j, y-1)) {
			left_[2*N-1] = p[y-1][x+j];
			found = true;
		}
	}

	// extreme case: no samples available
	if (!found)
		return;

	// if p[i][-1] (i=2*N-2:-1) is unavailable, substitute with reference below
	for (int i = 2*N-2; i >= 0; --i)
		left_[i] = context.available(x-1, y+i) ? p[y+i][x-1] : left_[i+1];

	corner_ = context.available(x-1, y-1) ? p[y-1][x-1] : left_[0];

	// if p[-1][j] (j=0:2*N-1) is unavailable, substitute with reference to its left
	top_[0] = context.available(x+0, y-1) ? p[y-1][x+0] : corner_;

	for (int j = 1; j <= 2*N-1; ++j)
		top_[j] = context.available(x+j, y-1) ? p[y-1][x+j] : top_[j-1];
}

const ReferenceSamples ReferenceSamples::filter(Mode mode) const {
	// filtered samples go here
	ReferenceSamples p(*this);
	const auto& N = block_side_;

	// small blocks and DC mode need no pre-processing filters
	if (mode == DC || N <= 4)
		return p;

	// some combinations of prediction mode and block sizes are skipped
	switch (N) {
		case 8:
			if (mode != ANGULAR_2 && mode != ANGULAR_18 && mode != ANGULAR_34)
				return p;
			break;

		case 16:
			if (mode == ANGULAR_9  || mode == ANGULAR_10 || mode == ANGULAR_11
			 || mode == ANGULAR_25 || mode == ANGULAR_26 || mode == ANGULAR_27
			) {
				return p;
			}
			break;

		case 32:
			if (mode == ANGULAR_10 || mode == ANGULAR_26) {
				return p;

			// if 32x32 blocks' refs are found to be "sufficiently flat"
			} else if (std::abs(at(-1, -1) + at(2*N-1, -1) -2*at(N-1, -1)) < (1 << (bit_depth()-5))
			           && std::abs(at(-1, -1) + at(-1, 2*N-1) -2*at(-1, N-1)) < (1 << (bit_depth()-5)))
			{
				// a linear interpolation filter is applied
				using hevc::math::lerp_round;

				for (int y = 0; y <= 62; ++y)
					p.at(-1, y) = lerp_round(y, -1, 63, at(-1, -1), at(-1, 63));

				for (int x = 0; x <= 62; ++x)
					p.at(x, -1) = lerp_round(x, -1, 63, at(-1, -1), at(63, -1));

				return p;
			}
			break;

		default:
			return p;
	}

	// by default, a three-tap [1 2 1] separable filter (with rounding) is applied
	p.at(-1, -1) = (at(-1, 0) + 2*at(-1, -1) + at(0,-1) + 2) / 4;

	for (int y = 0; y <= 2*N-2; ++y)
		p.at(-1, y) = (at(-1, y+1) + 2*at(-1, y) + at(-1, y-1) + 2) / 4;

	for (int x = 0; x <= 2*N-2; ++x)
		p.at(x, -1) = (at(x+1, -1) + 2*at(x, -1) + at(x-1, -1) + 2) / 4;

	return p;
}

// sample displacement in 1/32 fractions of the NxN block, for each angular mode
static inline int angle(Mode mode) {
	switch (mode) {
		case ANGULAR_2: return 32;
		case ANGULAR_3: return 26;
		case ANGULAR_4: return 21;
		case ANGULAR_5: return 17;
		case ANGULAR_6: return 13;
		case ANGULAR_7: return 9;
		case ANGULAR_8: return 5;
		case ANGULAR_9: return 2;
		case ANGULAR_10: return 0;
		case ANGULAR_11: return -2;
		case ANGULAR_12: return -5;
		case ANGULAR_13: return -9;
		case ANGULAR_14: return -13;
		case ANGULAR_15: return -17;
		case ANGULAR_16: return -21;
		case ANGULAR_17: return -26;
		case ANGULAR_18: return -32;
		case ANGULAR_19: return -26;
		case ANGULAR_20: return -21;
		case ANGULAR_21: return -17;
		case ANGULAR_22: return -13;
		case ANGULAR_23: return -9;
		case ANGULAR_24: return -5;
		case ANGULAR_25: return -2;
		case ANGULAR_26: return 0;
		case ANGULAR_27: return 2;
		case ANGULAR_28: return 5;
		case ANGULAR_29: return 9;
		case ANGULAR_30: return 13;
		case ANGULAR_31: return 17;
		case ANGULAR_32: return 21;
		case ANGULAR_33: return 26;
		case ANGULAR_34: return 32;
		default: assert(0); return 0;
	}
}

// equivalent to round(8192 / A)
static inline int inverse_angle(int a) {
	switch (a) {
		case -32: return -256;
		case -26: return -315;
		case -21: return -390;
		case -17: return -482;
		case -13: return -630;
		case -9: return -910;
		case -5: return -1638;
		case -2: return -4096;
		default: assert(0); return 0;
	}
}

const pixel& ReferenceSamples::ref(int pos, Mode mode) const {
	const auto A = angle(mode);
	const bool horizontal = mode < ANGULAR_18;

	// when A is positive samples are simply copied from prediction direction,
	// meanwhile, samples with negative indexes are obtained by projecting one
	// of the reference vectors to extend the other
	if (pos >= 0) {
		return horizontal ? at(-1, -1 + pos)
		/*else,vertical*/ : at(-1 + pos, -1);
	} else {
		const auto B = inverse_angle(A);
		const auto projection = (pos*B + 128) / 256;
		return horizontal ? at(-1 + projection, -1)
		/*else,vertical*/ : at(-1, -1 + projection);
	}
}

const ReferenceSamples& ReferenceSamples::predict(Block& b, Mode m) const {
	// only square blocks compatible with ref size
	const auto& N = static_cast<int>(block_side_);
	assert(b.width == b.height);
	assert(b.width == N);

	// planar prediction surface is generated by averaging hor. & vert.
	// predictions which reference block's top-right and bottom-left samples
	using hevc::math::lerp_round;
	if (m == PLANAR) {
		for (int y = 0; y < b.height; ++y) {
			for (int x = 0; x < b.width; ++x) {
				const pixel ph = lerp_round(x, -1, N-1, at(-1, y), at(N, -1));
				const pixel pv = lerp_round(y, -1, N-1, at(x, -1), at(-1, N));
				b[y][x] = (ph + pv + 1) / 2; // +1: rounding factor
			}
		}

	// predicted sample values are populated with the average of the
	// reference samples immediately left and to the above of the block
	} else if (m == DC) {
		unsigned long sum = 0;

		for (int j = 0; j < b.width; ++j)
			sum += at(j, -1);

		for (int i = 0; i < b.height; ++i)
			sum += at(-1, i);

		const pixel average = sum / (b.height + b.width);
		std::fill(b.begin(), b.end(), average);

	// block is predicted using a 1D sample ref vector accessed with angle parameter
	} else if (ANGULAR_2 <= m && m <= ANGULAR_34) {
		const auto A = angle(m);
		const bool horizontal = m < ANGULAR_18;

		// the interpolation is applied differently for horizontal and vertical modes
		if (horizontal) {
			for (int y = 0; y < b.height; ++y) {
				for (int x = 0; x < b.width; ++x) {
					const auto integer = ((x+1) * A) >> 5;
					const auto fractionary = ((x+1) * A) & 31;
					const auto pos = y + integer + 1;

					// when fractional parameter is zero, the interpolation can be omitted.
					// this is mandatory for angular modes 2 and 34 so as to avoid
					// accessing invalid reference samples during the evaluation of ref(pos+1)
					if (fractionary == 0)
						b[y][x] = ref(pos, m);
					else
						b[y][x] = lerp_round(fractionary, 0, 32, ref(pos, m), ref(pos+1, m));
				}
			}
		} else /* if (vertical) */ {
			for (int y = 0; y < b.height; ++y) {
				const auto integer = ((y+1) * A) >> 5;
				const auto fractionary = ((y+1) * A) & 31;

				for (int x = 0; x < b.width; ++x) {
					const auto pos = x + integer + 1;

					// see comment above on the necessity of this optimization
					if (fractionary == 0)
						b[y][x] = ref(pos, m);
					else
						b[y][x] = lerp_round(fractionary, 0, 32, ref(pos, m), ref(pos+1, m));
				}
			}
		}
	}

	return *this;
}

const ReferenceSamples& ReferenceSamples::smooth(Block& b, Mode mode) const {
	// for detail on clipping operations, see H.265 section 5.8 - Mathematical functions
	using hevc::math::saturate_cast;

	// prediction block sizes >= 32x32 are skipped
	if (b.width >= 32 || b.height >= 32) {
		return *this;

	// for the exactly vertical angular mode, smooth the left border
	} else if (mode == ANGULAR_26) {
		for (int y = 0; y < b.height; ++y)
			b[y][0] = saturate_cast<pixel>(b[y][0] + ((at(-1, y) - at(-1, -1)) >> 1));

	// for the exactly horizontal angular mode, smooth the upper border
	} else if (mode == ANGULAR_10) {
		for (int x = 0; x < b.width; ++x)
			b[0][x] = saturate_cast<pixel>(b[0][x] + ((at(x, -1) - at(-1, -1)) >> 1));

	// DC mode smooths all boundary samples
	} else if (mode == DC) {
		// in the corner, a three-tap [1 2 1] filter (with rounding) is applied
		b[0][0] = (at(-1, 0) + 2*b[0][0] + at(0, -1) + 2) / 4;

		// for the other boundary samples, a two-tap [1 3] filter suffices
		for (int x = 1; x < b.width; ++x)
			b[0][x] = (at(x, -1) + 3*b[0][x] + 2) / 4;

		for (int y = 1; y < b.height; ++y)
			b[y][0] = (at(-1, y) + 3*b[y][0] + 2) / 4;
	}

	return *this;
}

}
