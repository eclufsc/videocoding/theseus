#include "hevc/config.h"

#include <cassert>
#include <limits>


namespace hevc {

static short g_bit_depth = 8;

short bit_depth() {
	return g_bit_depth;
}

void bit_depth(short b) {
	assert(b > 0);
	assert(static_cast<unsigned>(1 << b) - 1 <= static_cast<unsigned>(std::numeric_limits<pixel>::max()));
	g_bit_depth = b;
}

pixel nominal_average() {
	return static_cast<pixel>(static_cast<unsigned>(1 << g_bit_depth) / 2);
}

}
