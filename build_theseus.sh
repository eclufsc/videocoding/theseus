function build() {
	mkdir build
	cd build
	cmake ..
	make
}

function clean() {
	rm -rf build
	rm -rf bin
}

function debug() {
	mkdir build
	cd build
	cmake -DCMAKE_BUILD_TYPE=Debug ..
	make
}


"$@"
