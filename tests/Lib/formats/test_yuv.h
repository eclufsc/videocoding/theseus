#include "formats/yuv.h"

#include <gtest/gtest.h>

#include "test_yuv_env.h"


namespace theseus::formats::test_yuv {

VideoCase1* const case1 = (VideoCase1*)::testing::AddGlobalTestEnvironment(new VideoCase1());

TEST(yuv, case1_f0_luma_read) {
	// Given
	// - inputs
	int frame_idx = 0;
	ChannelType channel = ChannelType::luma;
	// - expected outputs
	pixel* channel_data = case1->luma0;
	size_t width = case1->luma_w;
	size_t height = case1->luma_h;
	size_t size = width*height;

	// When
	pixel* container = new pixel[size];
	case1->format.read(container, frame_idx, channel, case1->file);

	// Expect
	EXPECT_TRUE(compare(channel_data, container, size));
	delete[] container;
}

TEST(yuv, case1_f1_luma_read) {
	// Given
	// - inputs
	int frame_idx = 1;
	ChannelType channel = ChannelType::luma;
	// - expected outputs
	pixel* channel_data = case1->luma1;
	size_t width = case1->luma_w;
	size_t height = case1->luma_h;
	size_t size = width*height;

	// When
	pixel* container = new pixel[size];
	case1->format.read(container, frame_idx, channel, case1->file);

	// Expect
	EXPECT_TRUE(compare(channel_data, container, size));
	delete[] container;
}

TEST(yuv, case1_f0_chroma_cb_read) {
	// Given
	// - inputs
	int frame_idx = 0;
	ChannelType channel = ChannelType::chroma_b;
	// - expected outputs
	pixel* channel_data = case1->cb0;
	size_t width = case1->chroma_w;
	size_t height = case1->chroma_h;
	size_t size = width*height;

	// When
	pixel* container = new pixel[size];
	case1->format.read(container, frame_idx, channel, case1->file);

	// Expect
	EXPECT_TRUE(compare(channel_data, container, size));
	delete[] container;
}

TEST(yuv, case1_f1_chroma_cb_read) {
	// Given
	// - inputs
	int frame_idx = 1;
	ChannelType channel = ChannelType::chroma_b;
	// - expected outputs
	pixel* channel_data = case1->cb1;
	size_t width = case1->chroma_w;
	size_t height = case1->chroma_h;
	size_t size = width*height;

	// When
	pixel* container = new pixel[size];
	case1->format.read(container, frame_idx, channel, case1->file);

	// Expect
	EXPECT_TRUE(compare(channel_data, container, size));
	delete[] container;
}

TEST(yuv, case1_f0_chroma_cr_read) {
	// Given
	// - inputs
	int frame_idx = 0;
	ChannelType channel = ChannelType::chroma_r;
	// - expected outputs
	pixel* channel_data = case1->cr0;
	size_t width = case1->chroma_w;
	size_t height = case1->chroma_h;
	size_t size = width*height;

	// When
	pixel* container = new pixel[size];
	case1->format.read(container, frame_idx, channel, case1->file);

	// Expect
	EXPECT_TRUE(compare(channel_data, container, size));
	delete[] container;
}

TEST(yuv, case1_f1_chroma_cr_read) {
	// Given
	// - inputs
	int frame_idx = 1;
	ChannelType channel = ChannelType::chroma_r;
	// - expected outputs
	pixel* channel_data = case1->cr1;
	size_t width = case1->chroma_w;
	size_t height = case1->chroma_h;
	size_t size = width*height;

	// When
	pixel* container = new pixel[size];
	case1->format.read(container, frame_idx, channel, case1->file);

	// Expect
	EXPECT_TRUE(compare(channel_data, container, size));
	delete[] container;
}

}
