#include <gtest/gtest.h>
#include <stdio.h>

#include "common/types.h"


namespace theseus::formats::test_yuv {

bool compare(const pixel* a, const pixel* b, size_t size) {
	for (int i = 0; i < size; i++) {
		if (a[i] != b[i]) {
			return false;
		}
	}
	return true;
};

class VideoCase1 : public ::testing::Environment{
public:
	pixel* luma0;
	pixel* luma1;
	pixel* cb0;
	pixel* cb1;
	pixel* cr0;
	pixel* cr1;
	size_t luma_w{126};
	size_t luma_h{80};
	size_t chroma_w{126>>1};
	size_t chroma_h{80>>1};
	structures::FrameDescriptor case1_frame_desc{luma_w, luma_h, c420};
	std::string case1_file_path{"./tests/input_files/w126_h80_f2_s420_bpp8.yuv"};
	Yuv format{case1_frame_desc};
	std::ifstream file;

	VideoCase1() {
		init_pointers();
		init_luma0_pattern();
		init_luma1_pattern();
		init_cb0_pattern();
		init_cb1_pattern();
		init_cr0_pattern();
		init_cr1_pattern();
		file.open(case1_file_path, std::ios::binary);
	}

	~VideoCase1() {
		delete[] luma0;
		delete[] luma1;
		delete[] cb0;
		delete[] cb1;
		delete[] cr0;
		delete[] cr1;
	}

private:
	void init_pointers() {
		luma0 = new pixel[luma_w*luma_h];
		luma1 = new pixel[luma_w*luma_h];
		cb0 = new pixel[chroma_w*chroma_h];
		cb1 = new pixel[chroma_w*chroma_h];
		cr0 = new pixel[chroma_w*chroma_h];
		cr1 = new pixel[chroma_w*chroma_h];
	}

	void init_luma0_pattern() {
		unsigned idx = 0;
		for (int i = 0; i < luma_h; i++) {
			for (int j = 0; j < luma_w; j++) {
				unsigned value = (unsigned)j/(luma_w >> 1);
				char pixel = 50 + 100*value;
				luma0[idx++] = pixel;
			}
		}
	}

	void init_luma1_pattern() {
		unsigned idx = 0;
		for (int i = 0; i < luma_h; i++) {
			for (int j = 0; j < luma_w; j++) {
				unsigned value = ((unsigned)i/(luma_h>>1) + (unsigned)j/(luma_w>>1)) % 2;
				char pixel = 50 + 100*value;
				luma1[idx++] = pixel;
			}
		}
	}

	void init_cb0_pattern() {
		unsigned idx = 0;
		for (int i = 0; i < chroma_h; i++) {
			for (int j = 0; j < chroma_w; j++) {
				unsigned value = 1 - (unsigned)i/(luma_h>>2);
				char pixel = 50 + 20*value;
				cb0[idx++] = pixel;
			}
		}
	}

	void init_cb1_pattern() {
		unsigned idx = 0;
		for (int i = 0; i < chroma_h; i++) {
			for (int j = 0; j < chroma_w; j++) {
				char pixel = 25;
				cb1[idx++] = pixel;
			}
		}
	}

	void init_cr0_pattern() {
		unsigned idx = 0;
		for (int i = 0; i < chroma_h; i++) {
			for (int j = 0; j < chroma_w; j++) {
				unsigned value = (unsigned)j/(luma_w>>2);
				char pixel = 50 + 20*value;
				cr0[idx++] = pixel;
			}
		}
	}

	void init_cr1_pattern() {
		unsigned idx = 0;
		for (int i = 0; i < chroma_h; i++) {
			for (int j = 0; j < chroma_w; j++) {
				unsigned value = (unsigned)i/(luma_w>>4);
				char pixel = 50 + 20*value;
				cr1[idx++] = pixel;
			}
		}
	}
};

}
