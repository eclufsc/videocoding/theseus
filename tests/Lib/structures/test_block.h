#include "structures/block.h"
#include "test_block_env.h"

#include <gtest/gtest.h>
#include <vector>
#include <algorithm>
#include <numeric>

#include "common/types.h"


namespace theseus::structures::test_Block {

constexpr size_t HEIGHT = 8;
constexpr size_t WIDTH = 16;
auto* test_frame = (TestFrame<HEIGHT,WIDTH>*)::testing::AddGlobalTestEnvironment(new TestFrame<HEIGHT,WIDTH>);

TEST(Block, ConstructInvalid) {
	// create block with invalid parameters
	ASSERT_DEATH(auto bInvalid = Block(nullptr, 0, 0, 0);, ".*Assertion.*");
	// ASSERT_DEATH(auto bInvalidData = Block(nullptr, WIDTH, HEIGHT);, ".*Assertion.*");
	// ASSERT_DEATH(auto bInvalidStride = Block(&test_frame->at(0, 0), WIDTH, HEIGHT, 0);, ".*Assertion.*");
	// ASSERT_DEATH(auto bInvalidWidth = Block(&test_frame->at(0, 0), 0, HEIGHT);, ".*Assertion.*");
	// ASSERT_DEATH(auto bInvalidHeight = Block(&test_frame->at(0, 0), WIDTH, 0);, ".*Assertion.*");
}

TEST(Block, ConstructFromFrame) {
	// create frame-sized block
	Block block(&test_frame->at(0, 0), test_frame->width(), test_frame->height());

	// test matching attributes
	EXPECT_EQ(&test_frame->at(0, 0), block.data);
	EXPECT_EQ(HEIGHT, block.height);
	EXPECT_EQ(WIDTH, block.width);

	// test some pixel locations
	EXPECT_EQ(255, block[0][0]);
	EXPECT_EQ(0, block[block.height-1][block.width-1]);
}

TEST(Block, ConstructFromRegion) {
	// create less-than-frame-sized block
	Block block(&test_frame->at(0, 0), test_frame->width() / 2, test_frame->height() / 2, test_frame->width());

	// test attributes
	EXPECT_EQ(&test_frame->at(0, 0), block.data);
	EXPECT_EQ(HEIGHT / 2, block.height);
	EXPECT_EQ(WIDTH / 2, block.width);

	// test some pixel locations
	for (int i = 0; i < block.height; ++i)
		for (int j = 0; j < block.width; ++j)
			EXPECT_EQ(255, block[i][j]);

	// bounds-checking
	ASSERT_DEATH(auto ubPixel = block[HEIGHT][0];, ".*Assertion.*");
}

TEST(Block, MoveAnchor) {
	// create block at one location (same as ConstructFromRegion test)
	Block block(&test_frame->at(0,0), test_frame->width() / 2, test_frame->height() / 2, test_frame->width());

	// anchor it somewhere else
	block.data = &test_frame->at(HEIGHT / 2, WIDTH / 2);

	// test attributes
	EXPECT_EQ(&test_frame->at(HEIGHT / 2, WIDTH / 2), block.data);
	EXPECT_EQ(HEIGHT / 2, block.height);
	EXPECT_EQ(WIDTH / 2, block.width);

	// test new pixel locations
	for (int i = 0; i < block.height; ++i)
		for (int j = 0; j < block.width; ++j)
			EXPECT_EQ(0, block[i][j]);

	// bounds-checking
	ASSERT_DEATH(auto ubPixel = block[-1][0];, ".*Assertion.*");
}

TEST(Block, Iterator) {
	// given
	Block block(&test_frame->at(0, 0), test_frame->width(), test_frame->height());

	// test relations between begin() and end()
	EXPECT_TRUE(block.begin() < block.end());
	EXPECT_TRUE(block.end() > block.begin());
	EXPECT_EQ(block.size(), block.end() - block.begin());
	EXPECT_EQ(block.end(), block.begin() + block.size());
	EXPECT_EQ(block.size(), std::distance(block.begin(), block.end()));

	// test random access
	for (int i = 0; i < block.height; ++i)
		for (int j = 0; j < block.width; ++j)
			EXPECT_EQ(block[i][j], block.begin()[i*block.width + j]);

	// test range-for and copy contents to a std::vector
	auto vectorized = std::vector<pixel>();
	vectorized.reserve(block.size());
	for (auto pixel : block)
		vectorized.push_back(pixel);

	// compare with vector (using <algorithm>'s equal)
	EXPECT_EQ(vectorized.size(), block.size());
	EXPECT_TRUE(std::equal(vectorized.begin(), vectorized.end(), block.begin(), block.end()));
}

TEST(Block, StandardLibraryAlgorithms) {
	{ // iteration with simple reads
		Block block(&test_frame->at(0, WIDTH / 2), test_frame->width() / 2, test_frame->height() / 2, test_frame->width());
		EXPECT_EQ((WIDTH*HEIGHT)/8, std::count(block.begin(), block.end(), 255));
	}

	{ // iteration with simple writes (and then reads)
		Block block(&test_frame->at(0, 0), test_frame->width() / 2, test_frame->height() / 2, test_frame->width());
		std::fill(block.begin(), block.end(), 0);
		std::for_each(block.begin(), block.end(), [](auto px){ EXPECT_EQ(0, px); });
	}

	{ // algorithms that apply more complex modifications
		Block block(&test_frame->at(HEIGHT / 2, 0), test_frame->width() / 2, test_frame->height() / 2, test_frame->width());

		// save original
		auto original = std::vector<pixel>();
		original.reserve(block.size());
		std::copy(block.begin(), block.end(), std::back_inserter(original));
		EXPECT_TRUE(std::equal(original.begin(), original.end(), block.begin(), block.end()));

		// block is now reversed (which is its inverse in this case)
		std::reverse(block.begin(), block.end());

		// calculate average block
		auto average = std::vector<pixel>();
		average.reserve(block.size());
		std::transform(
			block.begin(), block.end(),
			original.begin(),
			average.begin(),
			[](auto a, auto b){ return (a + b) / 2; }
		);
		std::for_each(average.begin(),average.end(), [](auto px){ EXPECT_EQ(127, px); });

		// SAD: Sum of Absolute Differences
		int sad = std::inner_product(
			block.begin(), block.end(),
			original.begin(),
			0,
			[](auto acc, auto abs_diff){ return acc + abs_diff; },
			[](auto a, auto b){ return std::abs(a - b); }
		);
		EXPECT_EQ(block.size()*255, sad);
	}
}

}
