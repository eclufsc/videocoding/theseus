#include "structures/buffer.h"
#include "test_buffer_env.h"

#include <gtest/gtest.h>
#include <algorithm>

#include "common/types.h"


namespace theseus::structures::test_buffer {

Samples* const samples = (Samples*)::testing::AddGlobalTestEnvironment(new Samples());

TEST(buffer, size1_insertion) {
	// Given
	Buffer<pixel> buffer{1, samples->size};

	//When
	pixel* pixels = buffer.next();
	std::copy_n(samples->luma0, samples->size, pixels);

	// Expect
	EXPECT_TRUE(compare(buffer.at(0), samples->luma0, samples->size));
}

TEST(buffer, size1_override) {
	// Given
	Buffer<pixel> buffer{1, samples->size};
	pixel* pixels = buffer.next();
	std::copy_n(samples->luma0, samples->size, pixels);

	//When
	pixels = buffer.next();
	std::copy_n(samples->luma1, samples->size, pixels);

	// Expect
	EXPECT_TRUE(compare(buffer.at(0), samples->luma1, samples->size));
}

TEST(buffer, size2_insertion) {
	// Given
	Buffer<pixel> buffer{2, samples->size};

	//When
	pixel* pixels = buffer.next();
	std::copy_n(samples->luma0, samples->size, pixels);
	pixels = buffer.next();
	std::copy_n(samples->luma1, samples->size, pixels);

	// Expect
	EXPECT_TRUE(compare(buffer.at(0), samples->luma0, samples->size));
	EXPECT_TRUE(compare(buffer.at(1), samples->luma1, samples->size));
}

TEST(buffer, size2_override) {
	// Given
	Buffer<pixel> buffer{2, samples->size};
	pixel* pixels = buffer.next();
	std::copy_n(samples->luma0, samples->size, pixels);
	pixels = buffer.next();
	std::copy_n(samples->luma1, samples->size, pixels);

	//When
	pixels = buffer.next();
	std::copy_n(samples->luma2, samples->size, pixels);

	// Expect
	EXPECT_TRUE(compare(buffer.at(0), samples->luma2, samples->size));
	EXPECT_TRUE(compare(buffer.at(1), samples->luma1, samples->size));
}

TEST(buffer, access_lower_idx) {
	// Given
	Buffer<pixel> buffer{1, samples->size};

	//When
	ASSERT_DEATH(buffer.at(-1);, ".*Assertion.*");
}

TEST(buffer, access_higher_idx) {
	// Given
	Buffer<pixel> buffer{1, samples->size};

	//When
	ASSERT_DEATH(buffer.at(2);, ".*Assertion.*");
}

}
