#include "structures/frameDescriptor.h"

#include <gtest/gtest.h>

#include "common/types.h"


namespace theseus::structures::test_frameDescriptor {

TEST(FrameDescriptor, sub_c444) {
	// Given
	size_t luma_w = 4, luma_h=2;
	Subsampling sub = c444;

	//When
	FrameDescriptor frame(luma_w, luma_h, sub);

	// Expect
	EXPECT_EQ(4, frame.chroma_width);
	EXPECT_EQ(2, frame.chroma_height);
}

TEST(FrameDescriptor, sub_c440) {
	// Given
	size_t luma_w = 4, luma_h=2;
	Subsampling sub = c440;

	//When
	FrameDescriptor frame(luma_w, luma_h, sub);

	// Expect
	EXPECT_EQ(4, frame.chroma_width);
	EXPECT_EQ(1, frame.chroma_height);
}

TEST(FrameDescriptor, sub_c422) {
	// Given
	size_t luma_w = 4, luma_h=2;
	Subsampling sub = c422;

	//When
	FrameDescriptor frame(luma_w, luma_h, sub);

	// Expect
	EXPECT_EQ(2, frame.chroma_width);
	EXPECT_EQ(2, frame.chroma_height);
}

TEST(FrameDescriptor, sub_c420) {
	// Given
	size_t luma_w = 4, luma_h=2;
	Subsampling sub = c420;

	//When
	FrameDescriptor frame(luma_w, luma_h, sub);

	// Expect
	EXPECT_EQ(2, frame.chroma_width);
	EXPECT_EQ(1, frame.chroma_height);
}

TEST(FrameDescriptor, sub_c411) {
	// Given
	size_t luma_w = 4, luma_h=2;
	Subsampling sub = c411;

	//When
	FrameDescriptor frame(luma_w, luma_h, sub);

	// Expect
	EXPECT_EQ(1, frame.chroma_width);
	EXPECT_EQ(2, frame.chroma_height);
}

}
