#include "structures/block.h"

#include <gtest/gtest.h>
#include <ostream>
#include <cassert>
#include <memory>

#include "common/types.h"


namespace theseus::structures::test_Block {

template <size_t H, size_t W>
class TestFrame : public ::testing::Environment {
public:
	TestFrame():
		contents_(std::make_unique<pixel[]>(H*W))
	{
		for (int i = 0; i < H / 2; ++i) {
			// white region
			for (int j = 0; j < W / 2; ++j)
				contents_[i*W + j] = 255;

			// horizontal stripes
			for (int j = W / 2; j < W; ++j)
				contents_[i*W + j] = i % 2 == 0 ? 255 : 0;
		}

		for (int i = H / 2; i < H; ++i) {
			// vertical stripes
			for (int j = 0; j < W / 2; ++j)
				contents_[i*W + j] = j % 2 == 0 ? 255 : 0;

			//black region
			for (int j = W / 2; j < W; ++j)
				contents_[i*W + j] = 0;
		}
	}

	pixel& at(int y, int x) {
		assert(y >= 0);
		assert(y < H);
		assert(x >= 0);
		assert(x < W);
		return contents_[y*W + x];
	}

	constexpr size_t height() const {
		return H;
	}

	constexpr size_t width() const {
		return W;
	}

private:
	std::shared_ptr<pixel[]> contents_;
};

}


std::ostream& operator<<(std::ostream& out, const theseus::structures::Block& block) {
	auto k = 0;
	for (const auto& px : block) {
		out << (k % block.width != 0 ? ", " : k == 0 ? "[ " : " ]\n[ ") << px;
		++k;
	}
	out << " ]\n";
	return out;
}
