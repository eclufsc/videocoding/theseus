#include <gtest/gtest.h>

#include "common/types.h"


namespace theseus::structures::test_buffer {

static bool compare(const pixel* a, const pixel* b, size_t size) {
	for (int i = 0; i < size; i++) {
		if (a[i] != b[i]) {
			printf("i=%d, (%d, %d)\n", i, a[i], b[i]);
			return false;
		}
	}
	return true;
};

class Samples : public ::testing::Environment{
public:
	pixel* luma0;
	pixel* luma1;
	pixel* luma2;

	size_t luma_w{126};
	size_t luma_h{80};
	size_t size{luma_w*luma_h};

	Samples() {
		init_pointers();
		init_luma0_pattern();
		init_luma1_pattern();
		init_luma2_pattern();
	}

	~Samples() {
		delete[] luma0;
		delete[] luma1;
		delete[] luma2;
	}

private:
	void init_pointers() {
		luma0 = new pixel[luma_w*luma_h];
		luma1 = new pixel[luma_w*luma_h];
		luma2 = new pixel[luma_w*luma_h];
	}

	void init_luma0_pattern() {
		unsigned idx = 0;
		for (int i = 0; i < luma_h; i++) {
			for (int j = 0; j < luma_w; j++) {
				unsigned value = (unsigned)j/(luma_w>>1);
				char pixel = 50 + 100*value;
				luma0[idx++] = pixel;
			}
		}
	}

	void init_luma1_pattern() {
		unsigned idx = 0;
		for (int i = 0; i < luma_h; i++) {
			for (int j = 0; j < luma_w; j++) {
				unsigned value = ((unsigned)i/(luma_h>>1) + (unsigned)j/(luma_w>>1)) % 2;
				char pixel = 50 + 100*value;
				luma1[idx++] = pixel;
			}
		}
	}

	void init_luma2_pattern() {
		unsigned idx = 0;
		for (int i = 0; i < luma_h; i++) {
			for (int j = 0; j < luma_w; j++) {
				unsigned value = ((unsigned)i/(luma_h>>1) + (unsigned)j/(luma_w>>1)) % 2;
				char pixel = 100*value;
				luma1[idx++] = pixel;
			}
		}
	}
};

}
