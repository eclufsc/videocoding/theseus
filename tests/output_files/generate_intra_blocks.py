#!/usr/bin/env python
import cv2 as cv
import numpy as np
import sys
import subprocess
from ast import literal_eval

block_size = 8
modes = sys.argv[1:]

for mode in modes:
	try:
		cmd = ['./bin/hevc_intra', str(block_size), mode]
		intra_output = subprocess.check_output(cmd)
	except subprocess.CalledProcessError as error:
		print(error.output)
		exit(error.returncode)

	block_raw = literal_eval(intra_output.decode())
	block_matrix = np.array(block_raw, dtype=np.uint8)
	print(block_matrix)
	image = cv.cvtColor(block_matrix, cv.COLOR_GRAY2BGR)

	cv.imwrite('./tests/output_files/'+mode+'.png', image)
