import numpy as np
import matplotlib.pyplot as plt

def read_channel(data, w, h, frame_idx, yuv_size, off_set):
	initial_position = yuv_size*frame_idx + off_set
	frame = [[0 for x in range(w)] for y in range(h)]
	
	idx = 0
	for i in range(h):
		for j in range(w):
			frame[i][j] = data[initial_position+idx]
			idx+=1

	# for i in range(h):
	# 	line = [ frame[i][j] for j in range(w) ]
	# 	print(line)

	return frame

# video specifications
frames = 2
luma_w = 126
luma_h = 80
chroma_w = luma_w>>1
chroma_h = luma_h>>1 
yuv_size = (luma_w*luma_h) + 2*(chroma_w*chroma_h)

# read data
file = open('w126_h80_f2_s420_bpp8.yuv', 'rb')
data = np.fromfile(file, dtype=np.uint8)

frame_idx = 0
luma_0 = read_channel(data, luma_w, luma_h, frame_idx, yuv_size, 0)
chroma_cb_0 = read_channel(data, chroma_w, chroma_h, frame_idx, yuv_size, luma_w*luma_h + chroma_w*chroma_h)
chroma_cr_0 = read_channel(data, chroma_w, chroma_h, frame_idx, yuv_size, luma_w*luma_h + chroma_w*chroma_h)

frame_idx = 1
luma_1 = read_channel(data, luma_w, luma_h, frame_idx, yuv_size, 0)
chroma_cb_1 = read_channel(data, chroma_w, chroma_h, frame_idx, yuv_size, luma_w*luma_h + chroma_w*chroma_h)
chroma_cr_1 = read_channel(data, chroma_w, chroma_h, frame_idx, yuv_size, luma_w*luma_h + chroma_w*chroma_h)

# plot frame 
fig, ax = plt.subplots()
# im = ax.imshow(luma_0, cmap='gray', vmin=0, vmax=255)
# plt.show()
# im = ax.imshow(chroma_cb_0, cmap='gray', vmin=0, vmax=255)
# plt.show()
im = ax.imshow(chroma_cr_0, cmap='gray', vmin=0, vmax=255)
plt.show()

# im = ax.imshow(luma_1, cmap='gray', vmin=0, vmax=255)
# plt.show()
# im = ax.imshow(chroma_cb_1, cmap='gray', vmin=0, vmax=255)
# plt.show()
# im = ax.imshow(chroma_cr_1, cmap='gray', vmin=0, vmax=255)
# plt.show()
